package eu.seitzal.ah_monitor.server

import com.typesafe.config.Config
import org.http4s._
import org.http4s.syntax._
import org.http4s.dsl.io._
import org.http4s.implicits._
import org.http4s.server.blaze._
import doobie.implicits._
import cats.effect._
import eu.seitzal.http4s.upickle_interop._
import scala.util.Try

final class Routes(conf: Config, db: DBLink, blizz: BlizzLink) {
  val service = HttpRoutes.of[IO] {

    case GET -> Root =>
      Ok("AH monitor API server")

    case GET -> Root/"region"/region/"realms" =>
      Ok(blizz.getRealms(region))
        .debuggable

    case GET -> Root/"region"/region/"realms"/"watched" =>
      Ok(blizz.getWatchedRealms(region))
        .debuggable

    case GET -> Root/"region"/region/"realms"/"uris" =>
      Ok(blizz.getRealmUrls(region))
        .debuggable

    case GET -> Root/"region"/region/"realms"/"infos" =>
      Ok(blizz.getRealmInfos(region))
        .debuggable

    case GET -> Root/"region"/region/"item"/item =>
      Ok(blizz.getItemInfo(region, item))
        .debuggable

    case GET -> Root/"region"/region/"realm"/realm/"item"/item/"prices" =>
      Ok(db.getEntries(region, realm, item))
        .debuggable

  }.orNotFound

  val debugMode = conf.getBoolean("server.debug")

  implicit class ResponseDebugOps(io: IO[Response[IO]]) {
    def debuggable: IO[Response[IO]] =
      if (debugMode)
        io.handleErrorWith {
          ex: Throwable => InternalServerError(ujson.Obj(
            "message" -> ujson.Str(ex.getMessage),
            "type" -> ujson.Str(ex.getClass.toString),
            "stackTrace" -> ujson.Arr(ex.getStackTrace().map(el => ujson.Str(el.toString)): _*)
          ))
        }
      else io
  }
}

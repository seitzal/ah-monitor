package eu.seitzal.ah_monitor.server

import cats.effect._
import cats.implicits._
import org.http4s._
import org.http4s.server.middleware.CORS
import com.typesafe.config.ConfigFactory
import java.io.File
import org.http4s.server.blaze.BlazeServerBuilder

object NewServer extends IOApp {

  val configOption1 = new File("config.json")
  val configOption2 = new File("../config.json")

  val conf =
    if(configOption1.exists)
      ConfigFactory.parseFile(configOption1)
    else
      ConfigFactory.parseFile(configOption2)

  val db = new DBLink(conf)
  val blizz = new BlizzLink(conf)

  def run(args: List[String]): IO[ExitCode] =
    BlazeServerBuilder[IO]
      .bindHttp(conf.getInt("server.port"), conf.getString("server.host"))
      .withHttpApp(CORS(new Routes(conf, db, blizz).service))
      .serve
      .compile
      .drain
      .as(ExitCode.Success) 

}

package eu.seitzal.ah_monitor.server

import org.http4s._
import org.http4s.headers._
import org.http4s.implicits._
import org.http4s.client._
import org.http4s.client.blaze._
import org.http4s.client.dsl.io._
import org.http4s.Method._
import eu.seitzal.http4s.upickle_interop._
import cats.Eval
import cats.effect._
import cats.implicits._
import com.typesafe.config.Config
import scala.collection.concurrent.TrieMap
import java.util.Base64

import scala.concurrent.ExecutionContext.global

final class BlizzLink(conf: Config) {

  private implicit val cs = IO.contextShift(global)
  private val realmLists = new TrieMap[String, List[ujson.Value]]

  private def withClient[A](f: Client[IO] => IO[A]) =
    BlazeClientBuilder[IO](global).resource.use(f)

  private def authenticate: IO[String] = withClient { client =>
    val clientId = conf.getString("snapshotter.identity.client_id")
    val secret = conf.getString("snapshotter.identity.secret")
    val rq = POST(
      UrlForm("grant_type" -> "client_credentials"),
      uri"https://eu.battle.net/oauth/token",
      Authorization(BasicCredentials(clientId, secret)))
    client.expect[ujson.Obj](rq)
      .map(_("access_token"))
      .map(_.asInstanceOf[ujson.Str].str)
  }

  private val token = Eval.later(authenticate.unsafeRunSync())

  private def authHeader: IO[Authorization] =
    IO(Authorization(Credentials.Token(AuthScheme.Bearer, token.value)))

  private def getJson(uriStr: String): IO[ujson.Value] = withClient {
    client =>
    for {
      auth   <- authHeader
      uri    <- IO.fromEither(Uri.fromString(uriStr))
      rq     <- GET(uri, auth)
      result <- client.expect[ujson.Value](rq)
    } yield result
  }

  def getItemInfo(region: String, item: String): IO[ujson.Value] =
    getJson(s"https://$region.api.blizzard.com/data/wow/item/$item?namespace=static-$region")

  def getRealmsRaw(region: String): IO[ujson.Value] =
    getJson(s"https://$region.api.blizzard.com/data/wow/connected-realm/index?namespace=dynamic-$region&region=$region")

  def getRealmUrls(region: String): IO[List[String]] =
    getJson(s"https://$region.api.blizzard.com/data/wow/connected-realm/index?namespace=dynamic-$region&region=$region")
      .map(_("connected_realms").asInstanceOf[ujson.Arr])
      .map(_.arr.toList)
      .map(_
        .map(_.asInstanceOf[ujson.Obj]("href"))
        .map(_.asInstanceOf[ujson.Str].str)
      )

  def getRealmInfos(region: String): IO[List[ujson.Value]] =
    getRealmUrls(region).flatMap(_.parTraverse(getJson))

  private def getRealmId(realmInfo: ujson.Value): IO[Int] =
    IO(realmInfo.asInstanceOf[ujson.Obj]("id"))
      .map(_.asInstanceOf[ujson.Num].num.toInt)
  
  private def getRealmCompoundName(realmInfo: ujson.Value): IO[String] =
    IO(realmInfo.asInstanceOf[ujson.Obj]("realms"))
      .map(_.asInstanceOf[ujson.Arr].arr.toList)
      .map(_
        .map(_.asInstanceOf[ujson.Obj]("name"))
        .map(_.asInstanceOf[ujson.Obj]("en_GB"))
        .map(_.asInstanceOf[ujson.Str].str)
      ).map(_.mkString(" / "))

  private def getRealmsUncached(region: String): IO[List[ujson.Value]] =
    getRealmInfos(region).flatMap(_.traverse{ realmInfo =>
      for {
        id           <- getRealmId(realmInfo)
        compoundName <- getRealmCompoundName(realmInfo)
      } yield ujson.Obj (
        "id"   -> id,
        "name" -> compoundName,
      )
    })

  def getRealms(region: String): IO[List[ujson.Value]] =
    if (realmLists.contains(region))
      IO(realmLists(region))
    else
      getRealmsUncached(region).map { realmList =>
        realmLists.putIfAbsent(region, realmList)
        realmList
      }

  lazy val watchedRealms = conf.getIntList("snapshotter.realms")

  def getWatchedRealms(region: String): IO[List[ujson.Value]] =
    getRealms(region)
      .map(_.filter(item => 
        watchedRealms.contains(
          item
          .asInstanceOf[ujson.Obj]("id")
          .asInstanceOf[ujson.Num].num.toInt)))

}

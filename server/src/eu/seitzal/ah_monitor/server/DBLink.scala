package eu.seitzal.ah_monitor.server

import doobie._
import doobie.implicits._
import doobie.util.ExecutionContexts
import cats._
import cats.effect._
import cats.implicits._
import com.typesafe.config.Config

final class DBLink(conf: Config) {

  implicit val cs = IO.contextShift(ExecutionContexts.synchronous)

  val transactor = Transactor.fromDriverManager[IO](
    "org.postgresql.Driver",
    "jdbc:postgresql://" +
      conf.getString("db.host") +
      ":" + conf.getInt("db.port") +
      "/" + conf.getString("db.name"),
    conf.getString("db.user"),
    conf.getString("db.password"),
    Blocker.liftExecutionContext(ExecutionContexts.synchronous)
  )

  def getEntries(region: String, realm: String, item: String): IO[List[Entry]] = 
    sql"""
        SELECT script_time, total_items, total_price, min_price, max_price, 
               median_price, avg_price
        FROM snapshots
        WHERE region = $region
        AND realm = ${realm.toInt}
        AND item = ${item.toInt}
        ORDER BY script_time"""
      .query[Entry]
      .stream
      .compile
      .toList
      .transact(transactor)

}
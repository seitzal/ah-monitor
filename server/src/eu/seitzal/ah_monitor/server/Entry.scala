package eu.seitzal.ah_monitor.server

import upickle.{default => json}

case class Entry (
  script_time: String,
  total_items: Int,
  total_price: Double,
  min_price: Double,
  max_price: Double,
  median_price: Double,
  avg_price: Double
)

object Entry {
  implicit val rw: json.ReadWriter[Entry] = json.macroRW
}

import mill._
import scalalib._

object server extends ScalaModule {
  def scalaVersion = "2.13.1"
  def ivyDeps = Agg(
    ivy"com.lihaoyi::upickle:0.9.5",
    ivy"org.http4s::http4s-dsl:0.21.2",
    ivy"org.http4s::http4s-blaze-server:0.21.2",
    ivy"org.http4s::http4s-blaze-client:0.21.2",
    ivy"eu.seitzal::http4s-upickle:0.1.0",
    ivy"org.tpolecat::doobie-core:0.8.8",
    ivy"org.tpolecat::doobie-postgres:0.8.8",
    ivy"com.typesafe:config:1.4.0"
  )
}
// Author: Alex Seitz <alex.seitz1@gmx.net
// License: MIT <https://mit-license.org/>
//
// A simple node.js script to retrieve, process and store price data from Blizzard's
// auction house API for World of Warcraft (retail edition)
// Last tested for WoW 8.3.0

// Require libraries
const ax = require('axios');
const fs = require('fs');
const pg = require('pg');

// Load configuration
const config = require("../config.json");

const region = config.snapshotter.region
const watchlist = config.snapshotter.realms
const identity = config.snapshotter.identity
const db = config.db

// Snapshot the time. This is done so data from the same script run can be associated later.
const script_time = new Date();
console.log(`Script time for this snapshot will be ${script_time} (unix: ${+script_time})`);

// Generate PostgreSQL connection string
const connStr = `postgres://${db.user}:${db.password}@${db.host}:${db.port}/${db.name}`;

// Connect to Postgres
const pgClient = new pg.Client({connectionString: connStr });
pgClient.connect();

// Create snapshots directory if it doesn't exist
if (!fs.existsSync("snapshots")){
  fs.mkdirSync("snapshots");
}

// Authenticate against Blizzard's API through OAuth2 client credentials flow,
// as detailed at https://develop.battle.net/documentation/guides/using-oauth/client-credentials-flow
console.log("Authenticating against Blizzard API...");
ax({
  method: "post",
  url: `https://${region}.battle.net/oauth/token`,
  headers: {"Content-Type": "application/x-www-form-urlencoded"},
  auth: {
    username: identity.client_id,
    password: identity.secret},
  data: "grant_type=client_credentials",
}).then((response) => {
  console.log("Authentication successful.");
  takeSnapshot(response.data.access_token);
}).catch((error) => {
  console.log("Authentication failed. See ah_monitor_error.txt for details.");
  fs.writeFile("ah_monitor_error.txt", error, (err) => {});
});

// Main method
function takeSnapshot(token) {
  // Obtain auction house data from Blizzard API for all realms on watchlist
  Promise.all(watchlist.map(realm => getDataForRealm(realm, token)))
    .then(returns => {
      // Process auction house data
      Promise.all(returns.map(tuple => processData(tuple[0], tuple[1])))
        .then(() => {
          // Terminate database client
          console.log("Processing complete. Terminating database client...");
          pgClient.end();
          console.log(`Snapshot successful. Took ${((new Date) - script_time) / 1000} seconds.`);
        }).catch(error => {
          console.log("Data processing failed. See ah_monitor_error.txt for details.");
          fs.writeFile("ah_monitor_error.txt", error.stack, (err) => {});
        });
    }).catch(error => {
      console.log("Data retrieval failed. See ah_monitor_error.txt for details.");
      fs.writeFile("ah_monitor_error.txt", error.stack, (err) => {});
    });
}

// Obtain auction house data from Blizzard API
async function getDataForRealm(realm, token) {
  console.log(`Retrieving data for ${region} realm #${realm}...`);
  const response = await ax({
    method: "get",
    url: `https://${region}.api.blizzard.com/data/wow/connected-realm/${realm}/auctions?namespace=dynamic-${region}`,
    headers: {"Authorization": "Bearer " + token}
  });
  console.log(`Data retrieval complete for ${region} realm #${realm}.`);
  return [realm, response.data];
}

// Helper functions for data processing

// Calculate unit price for a listing
function unit_price(listing) {
  return (listing.buyout != undefined ? listing.buyout : listing.unit_price) / 10000;
}

// Calculate total number of items of a type
function total_items(listings) {
  return listings
    .map(l => l.quantity)
    .reduce((a, b) => a + b);
}

// Calculate minimum price for an item type
function min_price(listings) {
  return listings
    .map(l => unit_price(l))
    .reduce((a, b) => a < b ? a : b);
}

// Calculate maximum price for an item type
function max_price(listings) {
  return listings
    .map(l => unit_price(l))
    .reduce((a, b) => a > b ? a : b);
}

// Calculate median price for an item type
function median_price(listings) {
  const threshold = total_items(listings) / 2;
  var count = 0;
  for(var i = 0; i < listings.length; i++) {
    count += listings[i].quantity;
    if (count >= threshold) {
      return unit_price(listings[i]);
    }
  }
}

// Calculate total price of all items of a type
function total_price(listings) {
  return listings
    .map(l => unit_price(l) * l.quantity)
    .reduce((a, b) => a + b);
}

// Process auction house data
async function processData(realm, data) {

  // Extract AH listings from HTTP response body
  const all_listings = data.auctions;

  // Identify unique item types
  const all_items = all_listings
    .map(l => l.item.id)
    .filter((value, index, self) => self.indexOf(value) === index);
  
    // Start processing
  console.log(`Processing data for ${all_items.length} unique items on ${region} realm #${realm}...`);
  var data = [];

  // Spawn a promise for each unique item type
  await Promise.all(all_items.map(async item => {

    // Extract listings for the item type
    const listings = all_listings
      .filter(l => l.item.id == item);

    // Calculate all data we need
    const totalItems = total_items(listings);
    const minPrice = +min_price(listings).toFixed(4);
    const medianPrice = +median_price(listings.sort((a, b) => unit_price(a) - unit_price(b))).toFixed(4);
    const maxPrice = +max_price(listings).toFixed(4);
    const totalPrice = +total_price(listings).toFixed(4);
    const avgPrice = +(totalPrice / totalItems).toFixed(4);

    // Store data in RAM (we write it to JSON later)
    data.push({
      item: item,
      total_items: totalItems,
      total_price: totalPrice,
      min_price: minPrice,
      max_price: maxPrice,
      median_price: medianPrice,
      avg_price: avgPrice
    });

    // Store the data in Postgres
    const query = {
      text: `INSERT INTO snapshots
        (script_time, region, realm, item, total_items, total_price, min_price, max_price, median_price, avg_price)
        VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10);`,
      values: [script_time, region, realm, item, totalItems, totalPrice, minPrice, maxPrice, medianPrice, avgPrice]
    };
    await pgClient.query(query);

  }));

  // Write the snapshot to JSON
  const fname = `snapshots/${realm}_${+script_time}.json`;
  const snapshot = JSON.stringify({
    realm: realm,
    time: +script_time,
    data: data
  }, null, 2);
  fs.writeFile(fname, snapshot, (err) => {});

  // Signal that we're done
  console.log(`Done processing data for ${region} realm #${realm}.`);
}

// The script terminates when all promises have completed.
// Promises are used for:
// - Authentication / Client credentials flow
// - REST API calls to get AH data
// - Database insert queries
